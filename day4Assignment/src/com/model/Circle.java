package com.model;

public class Circle extends Shape  {
private int radius;

public Circle(String name, int radius) {
	super(name);
	this.radius = radius;
}

public int getRadius() {
	return radius;
}

public void setRadius(int radius) {
	this.radius = radius;
}

@Override
public float calculateArea() {
	// TODO Auto-generated method stub
	float pi=3.14f;
	return (pi*this.radius*this.radius*100)/100;
}

}