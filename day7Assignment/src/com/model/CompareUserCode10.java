package com.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CompareUserCode10 {
public static String getDateDiffence(String s1,String s2) throws ParseException{
	SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-mm-yyyy");
	SimpleDateFormat simpleDateFormat2=new SimpleDateFormat("mm/dd/yyyy");
	Date date=simpleDateFormat.parse(s1);
	Date date2=simpleDateFormat.parse(s2);
	
	System.out.println("date1 is "+ simpleDateFormat.format(date));
	System.out.println("date2 is "+ simpleDateFormat.format(date2));
	if(date.compareTo(date2)>0) {
		return simpleDateFormat.format(date2);
	}
	else
	{
		return simpleDateFormat.format(date);
	}
}
}
