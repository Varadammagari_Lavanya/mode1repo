package com.main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.*;

import com.model.UserCoding;
public class MonthDiffMain {


		public static void main(String[] args) throws ParseException, IOException {
		             BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		             System.out.println("Enter the date in yyyy-mm-dd");
		             String s1 = br.readLine();
		             String s2 = br.readLine();
		             
		                System.out.println(UserCoding.getMonthDifference(s1, s2));
		}
		
		
	

}
