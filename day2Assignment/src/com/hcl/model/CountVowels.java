package com.hcl.model;
import java.util.Scanner;
public class CountVowels {
public int count() {
	Scanner scanner=new Scanner(System.in);
	System.out.println("Enter the String");
	String str=scanner.nextLine();
	str=str.toLowerCase();
	char[] c=str.toCharArray();
	int size=c.length;
	int v_count=0;
	for(int i=0;i<size;i++) {
		if(c[i]>='a'&&c[i]<='z') {
			if(c[i]=='a'||c[i]=='e'||c[i]=='i'||c[i]=='o'||c[i]=='u') {
				v_count++;
			}
		}
	}
	scanner.close();
	return v_count;
}
}
