package com.hcl.model;

public class Room {
 public int roomNo;
 public String roomType;
 public String roomArea;
 public String acMachine;
 public Room[] data;
 

public Room() {
	super();
}
public Room(int roomNo, String roomType, String roomArea, String acMachine) {
	super();
	this.roomNo = roomNo;
	this.roomType = roomType;
	this.roomArea = roomArea;
	this.acMachine = acMachine;
}
public int getRoomNo() {
	return roomNo;
}
public void setRoomNo(int roomNo) {
	this.roomNo = roomNo;
}
public String getRoomType() {
	return roomType;
}
public void setRoomType(String roomType) {
	this.roomType = roomType;
}
public String getRoomArea() {
	return roomArea;
}
public void setRoomArea(String roomArea) {
	this.roomArea = roomArea;
}
public String getAcMachine() {
	return acMachine;
}
public void setAcMachine(String acMachine) {
	this.acMachine = acMachine;
}
public Room[] getData() {
	return data;
}
public void setData(Room[] data) {
	this.data = data;
}
 
}
