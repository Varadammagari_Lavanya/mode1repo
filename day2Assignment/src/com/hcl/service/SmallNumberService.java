package com.hcl.service;
import java.util.Scanner;
public class SmallNumberService {
 public int findSmallestNumber() {
	 Scanner scanner=new Scanner(System.in);
	 System.out.println("Enter the array Length");
	 int length=scanner.nextInt();
	 int num[]=new int[length];
	 System.out.println("Enter Array Elements");
	 for (int i = 0; i < num.length; i++) {
		System.out.println("enter an element");
		num[i]=scanner.nextInt();
	}
	 
	 int temp;
	 for (int i = 0; i < num.length; i++) {
		 
		 for (int j = i+1; j < num.length; j++) {
			if(num[i]>num[j]) {
				temp=num[i];
				num[i]=num[j];
				num[j]=temp;
			}
		}
		
	}
	 scanner.close();
	return num[0];
 }
}
