package com.hcl.main;

import java.util.Scanner;

public class MiddleCharMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("enter the String");
		String string = scanner.next();

		PrintMiddlecharatacter(string);
		scanner.close();
	}

	public static void PrintMiddlecharatacter(String str) {
		int length = str.length();
		int middle;
		if (length % 2 != 0) {
			middle = length / 2;
		} else {
			middle = (length + 1) / 2;
		}

		System.out.println("The Middle character in the string :" + str.charAt(middle));

	}
}
