<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/nav.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<title>Add Pet</title>
<style>
body {
	margin: 0;
	font-family: Arial, Helvetica, sans-serif;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<ul class="nav navbar-nav">
			<li class="active"><a href="./allpets">Home</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><p style="color:#ffffff" class="navbar-text">Welcome, <%=(String)request.getSession().getAttribute("username") %></p></li>
			<li><a href="./userpets">My Pets</a></li>
			<li><a href="./addPet">Add Pet</a></li>
			<li><a href="./logout"><span
					class="glyphicon glyphicon-log-in"></span>Logout</a></li>
		</ul>
	</div>
	</nav>
	<div class="container">
		<br></br>
		<div id="addpet" style="margin-top: 50px;"
			class="mainbox col-md-5 col-md-offset-4 col-sm-8 col-sm-offset-2">
			<div class="panel-heading">
				<div class="panel-title">
					<div class="panel-title">
						<h4><b>Pet Information</b></h4>
					</div>
				</div>
			</div>
			<div style="padding-top: 10px" class="panel-body">
				<form:form   action="./savePet" class="form-horizontal" method="post" role="form" modelAttribute="petData">
					<div style="margin-bottom: 10px" class="input-group">
						<label for="Name">Pet Name</label><br>
						<form:input id="name" type="text"
							class="form-control" name="petname" path="name"
							placeholder="Enter pet name" size="30"/>
					</div>
					<div style="margin-bottom: 10px" class="input-group">
						<label for="NameDemo">Pet Age</label><br>
						<form:input id="age" type="text"
							class="form-control" name="petage" path="age" placeholder="Enter pet age" size="30"/>
					</div>
					<div style="margin-bottom: 10px" class="input-group">
						<label for="Place">Pet Place</label><br>
						<form:input id="place" type="text"
							class="form-control" name="petplace" path="place"
							placeholder="Enter pet place" size="30"/>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-0 col-sm-9 m-t-15">
							<button type="submit" class="btn btn-primary">Save</button>
							<button type="reset" class="btn btn-primary">Cancel</button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>