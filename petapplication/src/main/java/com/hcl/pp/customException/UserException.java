package com.hcl.pp.customException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserException extends Exception {

	private String message;
	private static final Logger LOGGER = LogManager.getLogger(UserException.class);
	
	public UserException(String message) {
		super();
		this.message = message;
		LOGGER.error("UserException: "+this.message);
	}

}
