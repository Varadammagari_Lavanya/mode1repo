package com.hcl.pp.service;

import java.util.List;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.dao.PetDao;
import com.hcl.pp.model.Pet;

@Service("petServiceImpl")
public class PetServiceImpl implements PetService {

	@Autowired
	private PetDao petDao;
	
	@Override
	@Transactional
	public void savePet(Pet pet) {
		
		if(pet!=null) {
			petDao.savePet(pet);
			Logger.getLogger("pet has saved");
		}
	}

	@Override
	@Transactional
	public List<Pet> getAllPets() throws UserException  {
		
		if(petDao.fetchAll().isEmpty()) {
			throw new UserException("Pets is not fetched properly");
		}
		else{
			return petDao.fetchAll();
		}
	}

	@Override
	public Pet getPetById(Long id) throws UserException {
		
		if(petDao.getPetById(id)!=null) {
			return petDao.getPetById(id);
		}
		else {
			throw new UserException("No pet is found by id");
		}
	}

}
