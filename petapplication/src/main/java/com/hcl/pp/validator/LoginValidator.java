package com.hcl.pp.validator;

import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.model.User;
import com.hcl.pp.service.SecurityService;

@Service("loginValidator")
public class LoginValidator {

	@Autowired
	private SecurityService  securityService ;
	public boolean validate(User user) throws UserException {
		
		boolean check = securityService.authenticateUser(user);
		if(check==true) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
