package com.hcl.pp.validator;

import com.hcl.pp.model.User;

public class UserValidator {

	public String validate(User user) {
		
		if(user!=null) {
			if(user.getUsername()==null) {
				return "Username should not be empty";
			}else if(user.getUserPassword()==null) {
				return "Password should not be empty";
			}
		}
		return "User has validated successfully";
	}
}
