package com.hcl.pp.dao;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;

@Repository("userDaoImpl")
public class UserDaoImpl implements UserDao {

	
	private static final Logger LOGGER = LogManager.getLogger(UserDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;
	

	Session session =null;
	Long userId;
	public Session getSession() {
		try {
			session = sessionFactory.getCurrentSession();
		}catch(Exception e) {
			session = sessionFactory.openSession();
		}
		return session;
	}
	public boolean addUser(User userDetails) throws UserException, SQLIntegrityConstraintViolationException {

		if (userDetails != null) {

			Session session1 =getSession();
			session1.save(userDetails);
			LOGGER.info("User added successfully");
			return true;
		}
		return false;
	}

	public boolean updateUser(User user) {

		if (user != null) {
			Session session1 =getSession();
			session1.saveOrUpdate(user);
			LOGGER.info("User updated  successfully");
			return true;
		}
		return false;
	}

	public List<User> listUsers() throws UserException {
		
		Session session1 =getSession();
		String hql = "FROM petuser";
		if (!hql.isEmpty()) {
			Query query = session1.createQuery(hql);
			List<User> users = query.getResultList();
			LOGGER.info("List of users retrieved");
			return users;
		}
		else {
			throw new UserException("No users found");
		}
	}

	public User getUserById(Long id) throws UserException {

		Session session1 =getSession();
		String hql = "from petuser p where p.id= :idVal";
		List<User> list=null;
		User user =new User();
		if (!hql.isEmpty()) {
			Query query = session1.createQuery(hql);
			query.setParameter("idVal", id);
			list = query.list();
			for (User userlist : list) {
				user.setId(id);
				user.setUsername(userlist.getUsername());
				user.setUserPassword(userlist.getUserPassword());
			}
			LOGGER.info("User is found by id");
		}
		else {
			throw new UserException("No user by id");
		}
		return user;
	}

	public User findByUserName(String username) throws UserException {

		Session session1 =getSession();
		String hql = "select * from petuser p where p.username= :name";
		User user = null;
		if (!hql.isEmpty()) {
			
			//Transaction transaction = session1.beginTransaction();
			List<User> list = session1.createNativeQuery(hql).setParameter("name", username).addEntity(User.class).getResultList();
			user = new User();
			for (User user2 : list) {
				System.out.println(user2.getId());
				user.setId(user2.getId());
				user.setUsername(user2.getUsername());
				user.setUserPassword(user2.getUserPassword());
			}
			System.out.println(user.getId());
			LOGGER.info("User is found by username");			
		}
		else {
			throw new UserException("No user find by username");
		}
		return user;
	}

	public boolean removeUser(User user) {

		Session session1 =getSession();
		session1.delete(user);
		LOGGER.info("User is removed successfully");
		return false;
	}

	public boolean authenticateUser(User userDetails) throws UserException {

		List<User> listUsers= listUsers();
		HttpServletRequest request = null;
		for (User user : listUsers) {
			if(user.getUsername().equals(userDetails.getUsername()) && user.getUserPassword().equals(userDetails.getUserPassword())) {
				LOGGER.info("User login successful");
				return true;			
			}
		}
		return false;

	}
	public boolean buyPet(Pet pet,Long userId) throws UserException {
		
		String hql = "update pets set ownerId = :id where PetId = :petId";
		if(pet!=null) {			
			Session session1 = getSession();		
			Transaction transaction = session1.beginTransaction();
			int result = session1.createNativeQuery(hql).setParameter("id", userId)
										  .setParameter("petId", pet.getId())
										  .addEntity(Pet.class)
										  .executeUpdate();
			LOGGER.info("userId:"+userId);
			LOGGER.info("petId:"+pet.getId());
			transaction.commit();
			if(result==1) {
				LOGGER.info("User has bought the pet..");
				return true;
			}
			else {
				throw new UserException("Owner id is not updated");
			}
		}
		return false;
	}

	public Set<Pet> getMyPets(String name) {
		
		String hql="select * from pets where ownerId = (select UserId from petuser where username= :name)";
		Set<Pet> pets = new HashSet<>();
		if(!hql.isEmpty()) {
			
			Session session1 = getSession();
			List<Pet> petList= session1.createNativeQuery(hql).setParameter("name", name).addEntity(Pet.class).getResultList();
			Pet newPet = null;
			for (Pet pet : petList) {
				newPet = new Pet(pet.getId(),pet.getName(),pet.getAge(),pet.getPlace());
				pets.add(newPet);
			}
			LOGGER.info("MyPets are retrieved by username");
		}
		return pets;
	}

}
