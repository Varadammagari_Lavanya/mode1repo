package com.hcl.pp.controller;

import java.lang.ProcessBuilder.Redirect;
import java.util.Set;

import javax.naming.Binding;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.dao.UserDao;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.SecurityService;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LoginValidator;

@Controller("userController")
public class UserController {

	private static final Logger LOGGER = LogManager.getLogger(UserController.class);
	
	@Autowired
	private PetService petService;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LoginValidator loginValidator; 
	
	
	@RequestMapping("user/add")
	public String addUser(Model model) {
		model.addAttribute("newUser",new User() );
		return "userRegister";
	}
	
	@PostMapping("user/register")
	public String userRegister(@ModelAttribute("newUser") User user,Model model) throws UserException {
		
		try {
			boolean value = userService.addUser(user);
			if(value==true) {
				LOGGER.info("User registration success");
				return "redirect:./loginPage";
			}
		}catch(Exception e) {
			throw new UserException("Username already exists");
		}
		return "redirect:/add";
	}
	
	@GetMapping("user/loginPage") 	
	public String login(Model model) {
		model.addAttribute("userData", new User());
		return "index";
	}
	@PostMapping("user/authenticate")
	public ModelAndView authenticateUser(HttpServletRequest request,@ModelAttribute("userData") User user,Model model) throws UserException{
			
			String name = request.getParameter("username");
			request.getSession().setAttribute("username", name);
			ModelAndView modelAndView = new ModelAndView();
			boolean check =  loginValidator.validate(user);
			if(check==true) {	
				modelAndView.setViewName("redirect:/allpets");
				modelAndView.addObject("message", "Login success");
			}
			else {
				modelAndView.setViewName("redirect:./loginPage");
				modelAndView.addObject("message","Either username or password is incorrect") ;
				LOGGER.error("User login unsuccessfull");
			}
			return modelAndView;
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request) {
		
		HttpSession session  = request.getSession();
		session.invalidate();
		return "redirect:./user/loginPage";
	}
	
	
	@GetMapping("/allpets")
	public String petsData(HttpServletRequest request,Model model) throws UserException
	{
		String username = (String)request.getSession().getAttribute("username");
		if(username==null) {
			request.setAttribute("Error", "Session has ended. Login again");
			return "redirect:./user/loginPage";
		}
		else {
			model.addAttribute("petHome",petService.getAllPets());
			return "home";
		}
	}
	
	@RequestMapping("petData/{value}")
	public String getPet( @PathVariable("value") Long id,Model model,HttpServletRequest request) throws UserException {
		
		String username = (String)request.getSession().getAttribute("username");
		User user = userService.findByUserName(username);
		Pet pet= petService.getPetById(id);
		System.out.println(user.getId());
		boolean check =userDao.buyPet(pet,user.getId());
		if(check==true) {
			LOGGER.info("Pet is successfully added");
		}
		else {
			throw new UserException("pet is not added");
		}
		return "redirect:/allpets";
	}
	
	@GetMapping("/userpets")
	public String myPets(Model model,HttpServletRequest request) {
		
		String username = (String)request.getSession().getAttribute("username");
		if(username==null) {
			request.setAttribute("Error", "Session has ended. Login again");
			return "redirect:./user/loginPage";
		}else {
			Set<Pet> userPet = userService.getMyPets(username);
			model.addAttribute("mypets", userPet);
			return "pets";
		}
	}
	
	@GetMapping("/addPet")
	public String addPet(HttpServletRequest request,Model model) {
		
		String username = (String)request.getSession().getAttribute("username");
		if(username==null) {
			request.setAttribute("Error", "Session has ended. Login again");
			return "redirect:./user/loginPage";
		}else {
			model.addAttribute("petData",new Pet());
			return "addPet";
		}
	}
	
	@PostMapping("/savePet")
	public String savePet(@ModelAttribute("petData") Pet pet,HttpServletRequest request) {
		
		String username = (String)request.getSession().getAttribute("username");
		if(username==null) {
			request.setAttribute("Error", "Session has ended. Login again");
			return "redirect:./user/loginPage";
		}else {
			petService.savePet(pet);
			return "redirect:/allpets";
		}
	}
	
			
}
