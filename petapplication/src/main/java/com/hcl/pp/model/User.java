package com.hcl.pp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity(name="petuser")
@Table(name="petuser")
public class User implements Serializable{

	@Id
	@Column(name="UserId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="Username",unique=true)
	@NotEmpty (message="All fields are mandatory")
	@Size(max=7)
	private String username;
	
	@Column(name="Password")
	@NotEmpty (message="All fields are mandatory")
	
	private String userPassword;
	
	private String confirmPassword;
	@OneToMany(cascade=CascadeType.ALL)
	private Set<Pet> pets;
	public User() {
		super();
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public User(Long id, @NotEmpty(message = "All fields are mandatory") @Size(min = 5, max = 10) String username,
			@NotEmpty(message = "All fields are mandatory") @Size(message = "All fields are mandatory") String userPassword) {
		super();
		this.id = id;
		this.username = username;
		this.userPassword = userPassword;
	}


	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public Set<Pet> getPets() {
		return pets;
	}
	public void setPets(Set<Pet> pets) {
		this.pets = pets;
	}
	
	
}
