package day1Assignment;

import java.util.Scanner;

public class SquareEvenDigits {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sumOfEven = sumOfSquaresOfEvenDigits();
		System.out.println("the sum of even digits in a given Number " + sumOfEven);
	}

	public static int sumOfSquaresOfEvenDigits() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		int num = scanner.nextInt();
		int digit;
		int sum = 0;
		while (num != 0) {
			digit = num % 10;
			if (digit % 2 == 0) {
				sum = sum + (digit * digit);
			}
			num = num / 10;
		}
		scanner.close();
		return sum;

	}
}
