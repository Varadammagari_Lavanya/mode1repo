package day1Assignment;

import java.util.Scanner;

public class SwapNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the numbers to swap");
		System.out.println("Enter the first number: ");
		int number1 = scanner.nextInt();
		System.out.println("Enter the second number:");
		int number2 = scanner.nextInt();
		int temp;
		System.out.println("The numbers before swapping are " + number1 + " and " + number2);
		temp = number1;
		number1 = number2;
		number2 = temp;
		System.out.println("The numbers after swapping are " + number1 + " and " + number2);
		scanner.close();

	}

}
