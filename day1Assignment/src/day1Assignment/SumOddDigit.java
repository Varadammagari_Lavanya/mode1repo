package day1Assignment;
import java.util.Scanner;
public class SumOddDigit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    int n=checkSum();
    if(n==1) {
    	System.out.println("Sum of odd digits is odd");
    }else {
    	System.out.println("Sum of odd digits is even ");
    }
   
	}
	public static int checkSum() {
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter the Number");
		int num=scanner.nextInt();
		int digit;
		int count=0;
		while(num!=0) {
			digit=num%10;
			if(digit%2!=0) {
				count++;
			}
			num=num/10;
		}
		scanner.close();

		if (count%2!=0) {
			return 1;
		}else {
			return -1;
		}
	}

}
