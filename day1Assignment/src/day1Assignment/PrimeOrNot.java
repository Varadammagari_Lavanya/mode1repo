package day1Assignment;

import java.util.Scanner;

public class PrimeOrNot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number to check prime or not");
		int num = scanner.nextInt();
		for (int i = 2; i < num; i++) {
			if (num % i == 0) {
				System.out.println("The given number is not Prime Number");
				System.exit(0);
			}
		}
		System.out.println("The given number is Prime Number");
		scanner.close();
	}

}
