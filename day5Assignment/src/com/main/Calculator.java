package com.main;

import com.model.MyCalculator;
import java.util.Scanner;

public class Calculator extends MyCalculator {
	public static void main(String[] args) {

		try {
			Scanner scanner = new Scanner(System.in);
           System.out.println("enter the n and p");
			int base = scanner.nextInt();
			int exponent = scanner.nextInt();

			System.out.println(power(base, exponent));

			scanner.close();
		} catch (ArithmeticException e) {
			System.out.println("java.lang.Exception:base and exponent should not be Zero");
		} catch (IllegalArgumentException e) {
			System.out.println("java.lang.Exception:base or exponent should not be negative");
		} catch (Exception e) {
			System.out.println("some issue has been occured");
		}
	}
}
